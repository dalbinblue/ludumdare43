namespace Dalbinblue.BaseGameLibrary.Audio
{
  public class SoundEffectOptions
  {
    /// <summary>
    /// Specifies how to handle playback of the sound effect file if a copy of
    /// the sound effect file is already playing when a copy of the same sound
    /// effect.
    /// </summary>
    public SoundEffectPlaybackMethod SoundEffectPlaybackMethod { get; set; }

    /// <summary>
    /// If true the sound manager will load the sound effect upon registration
    /// and keep it in memory until unregistered.  If false, the sound effect
    /// will be loaded when playback is called, and unloaded when playback is
    /// complete.  The default is to keep a sound effect file in memory; 
    /// however, one off sounds such as voice-overs should not be kept in 
    /// memory when not needed.
    /// </summary>
    public bool KeepSoundEffectInMemory { get; set; }

    public static readonly SoundEffectOptions DefaultSoundEffectOptions =
      new SoundEffectOptions
      {
        SoundEffectPlaybackMethod = SoundEffectPlaybackMethod.PlayAdditionalCopy,
        KeepSoundEffectInMemory = true
      };
  }
}