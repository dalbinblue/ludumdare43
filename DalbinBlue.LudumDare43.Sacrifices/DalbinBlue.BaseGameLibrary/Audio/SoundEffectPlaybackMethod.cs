namespace Dalbinblue.BaseGameLibrary.Audio
{
  public enum SoundEffectPlaybackMethod
  {
    /// <summary>
    /// The sound effect requested will only play if a copy of the same sound 
    /// effect is not already playing.  This is the default option.
    /// </summary>
    PlayIfNotAlreadyPlaying = 0,

    /// <summary>
    /// If the sound effect requested is already playing, the playing instance
    /// will be stopped and restarted from the beginning.
    /// </summary>
    RestartIfPlaying,

    /// <summary>
    /// If the sound effect request is already playing, a new copy will start
    /// such that are both playing simultaneously.  If the maximum configured
    /// number of simultaneous instances of sound effect are already playing,
    /// then no action will be performed when using this option.
    /// </summary>
    PlayAdditionalCopy
  }
}