using System;
using System.Collections.Generic;

namespace Dalbinblue.BaseGameLibrary.Data.Map {
  [Serializable]
  public class TileDetail {
    public Dictionary<string, string> Properties { get; set; }     
  }
}