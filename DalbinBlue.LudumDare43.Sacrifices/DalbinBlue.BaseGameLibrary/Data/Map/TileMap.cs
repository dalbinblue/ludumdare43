﻿using System;
using System.Collections.Generic;

namespace Dalbinblue.BaseGameLibrary.Data.Map {
  [Serializable]
  public class TileMap {
    public TileMapOrientation Orientation { get; set; }
    public int TileWidth { get; set; }
    public int TileHeight { get; set; }
    public int WidthInTiles { get; set; }
    public int HeightInTiles { get; set; }
    public Dictionary<string, string> Properties { get; set; } 
    public TileMapLayer[] Layers { get; set; }
    public TileMapTilesetDefinition[] Tilesets { get; set; }
    public TileMapObjectGroup[] ObjectGroups { get; set; }

    public int GetLayerIndexByName(string name) {
      for (int i = 0; i < Layers.Length; i++) {
        if (Layers[i].Name == name) return i;
      }
      return -1;
    }
  }
}
