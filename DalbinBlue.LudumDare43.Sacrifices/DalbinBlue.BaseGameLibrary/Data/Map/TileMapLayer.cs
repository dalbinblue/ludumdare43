﻿using System;
using System.Collections.Generic;

namespace Dalbinblue.BaseGameLibrary.Data.Map {
  [Serializable]
  public class TileMapLayer {
    public string Name { get; set; }
    public Dictionary<string, string> Properties { get; set; }
    public UInt32[][] LayerTiles { get; set; }
  }
}
