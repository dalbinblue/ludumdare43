using System;
using System.Collections.Generic;

namespace Dalbinblue.BaseGameLibrary.Data.Map {
  [Serializable]
  public class TileMapTilesetDefinition {
    public int FirstTileId { get; set; }
    public int TileWidth { get; set; }
    public int TileHeight { get; set; }
    public string Name { get; set; }
    public int Spacing { get; set; }
    public int Margin { get; set; }
    public string ImageAssetName { get; set; }
    public Dictionary<int, TileDetail> TileDetails { get; set; }
    public Dictionary<string, string> Properties { get; set; } 
  }
}