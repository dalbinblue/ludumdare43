using System;

namespace Dalbinblue.BaseGameLibrary.Data.Sprite {
  [Serializable]
  public class SpriteAnimationDefinition {
    public SpriteFrame[] Frames { get; set; }
    public int StartFrame { get; set; }
    public bool Loop { get; set; }
    public string JumpToAnimationOnEnd { get; set; }
    public int StartFrameOnLoop { get; set; }
  }
}