﻿using System;
using System.Collections.Generic;

namespace Dalbinblue.BaseGameLibrary.Data.Sprite {
  [Serializable]
  public class SpriteSheet {
    public string ImageAssetName { get; set; }
    public SpriteImageSlice[] ImageSlices { get; set; }
    public Dictionary<string, SpriteDefinition> Sprites { get; set; }
  }
}
