﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary {
  public static class GeometryExtensions {
    /// <summary>
    /// Calculates the minimum distance that source must move so that the two rectangles no longer collide.
    /// </summary>
    public static Point GetOffsetToUncollide(this Rectangle source, Rectangle other)
    {
      if (!source.Intersects(other)) return new Point(0,0);

      int shiftLeft = source.Right - other.Left;
      int shiftRight = other.Right - source.Left;
      int shiftUp = source.Bottom - other.Top;
      int shiftDown = other.Bottom - source.Top;

      if (shiftLeft <= 0) shiftLeft = int.MaxValue;
      if (shiftRight <= 0) shiftRight = int.MaxValue;
      if (shiftUp <= 0) shiftUp = int.MaxValue;
      if (shiftDown <= 0) shiftDown = int.MaxValue;

      if (shiftLeft < shiftRight)
      {
        if (shiftLeft < shiftUp)
        {
          return shiftLeft < shiftDown ? new Point(-shiftLeft, 0) : new Point(0, shiftDown);
        }
        else
        {
          return shiftUp < shiftDown ? new Point(0, -shiftUp) : new Point(0, shiftDown);
        }
      }
      else
      {
        if (shiftRight < shiftUp) {
          return shiftRight < shiftDown ? new Point(shiftRight,0) : new Point(0, shiftDown);
        }
        else {
          return shiftUp < shiftDown ? new Point(0, -shiftUp) : new Point(0, shiftDown);
        }        
      }
    }

    /// <summary>
    /// Calculates the minimum distance that source must move so that the two rectangles no longer collide.
    /// </summary>
    public static IEnumerable<Point> GetOffsetToUncollideCandidates(this Rectangle source, Rectangle other) {
      if (!source.Intersects(other)) return new List<Point>();

      var candidates = new List<Point>();

      int shiftLeft = source.Right - other.Left;
      int shiftRight = other.Right - source.Left;
      int shiftUp = source.Bottom - other.Top;
      int shiftDown = other.Bottom - source.Top;

      if (shiftLeft > 0) candidates.Add(new Point(-shiftLeft,0));
      if (shiftRight > 0) candidates.Add(new Point(shiftRight, 0));
      if (shiftUp > 0) candidates.Add(new Point(0, -shiftUp));
      if (shiftDown > 0) candidates.Add(new Point(0, shiftDown));

      if (shiftLeft > 0 && shiftUp > 0) candidates.Add(new Point(-shiftLeft, -shiftUp));
      if (shiftLeft > 0 && shiftDown > 0) candidates.Add(new Point(-shiftLeft, shiftDown));
      if (shiftRight > 0 && shiftUp > 0) candidates.Add(new Point(shiftRight, -shiftUp));
      if (shiftRight > 0 && shiftDown > 0) candidates.Add(new Point(shiftRight, shiftDown));
      
      return candidates.OrderBy(p => Math.Abs(p.X) + Math.Abs(p.Y));
    }

    public static Rectangle Shift(this Rectangle source, Point shiftAmount)
    {
      return new Rectangle(
        source.X + shiftAmount.X,
        source.Y + shiftAmount.Y,
        source.Width,
        source.Height);
    }

    public static Point Shift(this Point source, Point shiftAmount)
    {
      return new Point(source.X + shiftAmount.X, source.Y = source.Y + shiftAmount.Y);
    }

    public static Point Shift(this Point source, int x, int y)
    {
      return new Point(source.X + x, source.Y = source.Y + y);
    }

    public static Point GetBottomCenter(this Rectangle rectangle) {
        return new Point(rectangle.Center.X, rectangle.Bottom);
    }

    public static Vector2 Shift(this Vector2 source, Vector2 shift) {
      return new Vector2(source.X + shift.X, source.Y + shift.Y);
    }

    public static Vector2 Shift(this Vector2 source, float x, float y) {
      return new Vector2(source.X + x, source.Y + y);
    }

    public static Vector2 ToVector(this Point source) {
      return new Vector2(source.X, source.Y);
    }

    public static Point ToPoint(this Vector2 source) {
      return new Point(Convert.ToInt32(source.X), Convert.ToInt32(source.Y));
    }

  }
}
