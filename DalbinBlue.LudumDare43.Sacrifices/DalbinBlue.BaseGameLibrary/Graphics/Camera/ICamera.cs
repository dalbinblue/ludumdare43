using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Graphics.Camera {
  public interface ICamera {
    float Rotation { get; }
    float Zoom { get; }
    float X { get; }
    float Y { get; }

    void Update(GameTime gameTime);
  }
}