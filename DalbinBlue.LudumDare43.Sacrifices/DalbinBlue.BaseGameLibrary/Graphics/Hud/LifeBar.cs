﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics.Hud {
  public class LifeBar : RenderableObjectBase
  {
    private Texture2D _texture;


    public int MinValue { get; set; }
    public int MaxValue { get; set; }
    public int CurrentValue { get; set; }
   // public int MaxWidth { get; set; }
    public Rectangle TextureCoordinates { get; set; }

    public LifeBar(Texture2D texture, Rectangle textureCoordinates)
    {
      _texture = texture;
      TextureCoordinates = textureCoordinates;
      MinValue = 0;
      MaxValue = textureCoordinates.Width;
      Height = textureCoordinates.Height;
      CurrentValue = MaxValue;
     // MaxWidth = texture.Width;
    }

    public override void PostMovementUpdate(GameTime gameTime) {
      // DO NOTHING
    }

    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation) {
      var transformationMatrix = GetLocalTransformationMatrix() * parentTransformation;
      TransformationData transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

      int textureWidth = CurrentValue * TextureCoordinates.Width / MaxValue;
      var source = new Rectangle(TextureCoordinates.X, TextureCoordinates.Y, textureWidth, TextureCoordinates.Height);
      spriteBatch.Draw(_texture, transformationData.Position, source, Color, transformationData.Rotation, Origin, transformationData.Scale, SpriteEffects.None, Layer);
    }
  }
}

