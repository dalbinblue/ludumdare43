﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics.Hud
{
  public class RevealingTextDisplay : RenderableObjectBase {
    private readonly int _maximumWidth;
    private readonly SpriteFont _font;
    private string _text;
    private bool _isAnimating;
    private string[] _sourceLines;
    private string[] _revealedLines;
    private int _currentLine;
    private int _currentCharacter;
    private double _timeSinceLastReveal;

    public event EventHandler TextRevealed;

    protected virtual void OnTextRevealed() {
      EventHandler handler = TextRevealed;
      if (handler != null) handler(this, EventArgs.Empty);
    }

    public RevealingTextDisplay(SpriteFont font, int maximumWidth)
    {
      _maximumWidth = maximumWidth;
      _font = font;
      LineHeight = _font.LineSpacing;
      _text = string.Empty;
      RevealRate = 20;
    }

    public int LineHeight { get; set; }
    public double RevealRate { get; set; }
      public bool IsTextRevealed => !_isAnimating;

      public void SetTextToReveal(string text) {
      _sourceLines = SplitStringIntoLines(text).ToArray();
      _revealedLines = new string[_sourceLines.Length];
      for (int i = 0; i < _revealedLines.Length; i++) {
        _revealedLines[i] = string.Empty;
      }
      _currentLine = 0;
      _currentCharacter = 0;
      _isAnimating = true;
      _timeSinceLastReveal = 0.0;
    }

    public void RevealAllText() {
      if (_isAnimating) {
        _isAnimating = false;
        _revealedLines = _sourceLines;
        OnTextRevealed();
      }
    }

    public IEnumerable<string> SplitStringIntoLines(string text)
    {
      if (string.IsNullOrEmpty(text)) return new List<string>();

      var words = text.Split(' ');

      string currentLine = "";
      var lines = new List<string>();

      foreach (var word in words)
      {
        string newLine;

        if (currentLine == "")
        {
          newLine = word;
        }
        else
        {
          newLine = currentLine + " " + word;
        }

        if (_font.MeasureString(newLine).X > _maximumWidth)
        {
          lines.Add(currentLine);
          currentLine = word;
        }
        else
        {
          currentLine = newLine;
        }
      }

      if (currentLine != string.Empty)
      {
        lines.Add(currentLine);
      }

      return lines.ToArray();
    }

    public override void PreMovementUpdate(GameTime gameTime)
    {
      base.PreMovementUpdate(gameTime);

        if (_sourceLines == null || _sourceLines.Length == 0) return;

      double timePerCharacter = 1.0/RevealRate;
      _timeSinceLastReveal += gameTime.ElapsedGameTime.TotalSeconds;

      while (_isAnimating && _timeSinceLastReveal > timePerCharacter) {
        _timeSinceLastReveal -= timePerCharacter;

        _currentCharacter += 1;
        if (_currentCharacter >= _sourceLines[_currentLine].Length) {
          _revealedLines[_currentLine] = _sourceLines[_currentLine];
          _currentCharacter = 0;
          _currentLine += 1;
        }
        else if (_currentCharacter > 0) {
          _revealedLines[_currentLine] = _sourceLines[_currentLine].Substring(0, _currentCharacter);
        }

        if (_currentLine >= _sourceLines.Length) {
          _isAnimating = false;
          OnTextRevealed();
        }
      }
    }

    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation) {
      if (_revealedLines == null) return;
      int lineNumber = 0;
      foreach (var line in _revealedLines) {
        var transformationMatrix = Matrix.CreateTranslation(0f, lineNumber*LineHeight, 0f)*
                                   GetLocalTransformationMatrix()*parentTransformation;
        TransformationData transformationData = DecomposeTransformationMatrix(ref transformationMatrix);

        spriteBatch.DrawString(_font, line, transformationData.Position, Color, transformationData.Rotation, Origin,
                               transformationData.Scale, SpriteEffects.None, Layer);
        lineNumber += 1;
      }
    }
  }
}
