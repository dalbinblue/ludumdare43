﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dalbinblue.BaseGameLibrary.Scripting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics.Hud
{
  public class TextDialog : RenderableObjectBase {
    private BlinkingRenderableContainer _cursor;
    private RevealingTextDisplay _textDisplay;
    private BorderBox _borderBox;
    private bool _isAwaitingInput = false;
    private bool _isAllTextDisplayed = false;
    private int _maximumDisplayLines;
    private string[] _lines;
    private int _currentLineGroup;

    public bool IsAwaitingInput { get { return _isAwaitingInput; } }
    public bool IsAllTextDisplayed { get { return _isAllTextDisplayed; } }

    public event EventHandler AwaitingInput;

    protected virtual void OnAwaitingInput() {
      EventHandler handler = AwaitingInput;
      if (handler != null) handler(this, EventArgs.Empty);
    }

    public event EventHandler AllTextDisplayed;

    protected virtual void OnAllTextDisplayed() {
        foreach (var scriptState in _scriptsWaitingForAllTextDisplayed)
        {
            scriptState.IsComplete = true;
        }
        _scriptsWaitingForAllTextDisplayed = new List<ScriptState>();

      EventHandler handler = AllTextDisplayed;
      if (handler != null) handler(this, EventArgs.Empty);
    }

      private List<ScriptState> _scriptsWaitingForAllTextDisplayed = new List<ScriptState>();
      public ScriptState WaitForAllTextDisplayed()
      {
          var state = new ScriptState();
          if (!_isAllTextDisplayed)
          {
              _scriptsWaitingForAllTextDisplayed.Add(state);
          }
          else
          {
              state.IsComplete = true;
          }
          return state;
      }

        public TextDialog(int width, int height, 
      Texture2D borderTexture, 
      Rectangle borderTextureCoordinates, 
      int borderWidth, 
      Texture2D cursorTexture,
      Rectangle cursorTextureCoordinates,
      double blinkRate,
      SpriteFont font) 
    {
      _borderBox = new BorderBox(borderTexture, borderTextureCoordinates, width,height,borderWidth);  
      _borderBox.Position = new Vector2(0,0);
      _textDisplay = new RevealingTextDisplay(font, width - (2*borderWidth));
      _textDisplay.Position = new Vector2(borderWidth,borderWidth);
      _textDisplay.TextRevealed += _textDisplay_TextRevealed;
      int textBoxHeight = (height - ((2*borderWidth) + cursorTextureCoordinates.Height));
      _maximumDisplayLines = textBoxHeight /_textDisplay.LineHeight;
      StaticImage cursorImage = new StaticImage(cursorTexture, cursorTextureCoordinates);
      _cursor = new BlinkingRenderableContainer(cursorImage, blinkRate);
      _cursor.Position = new Vector2((width - 2 * borderWidth) / 2 - (cursorTextureCoordinates.Width / 2), height - borderWidth - cursorTextureCoordinates.Height);
    }

    void _textDisplay_TextRevealed(object sender, EventArgs e) {
      _isAwaitingInput = true;
      OnAwaitingInput();
    }

    public override void PreMovementUpdate(GameTime gameTime)
    {
      _borderBox.Update(gameTime);
      _textDisplay.Update(gameTime);
      _cursor.Update(gameTime);
      base.PreMovementUpdate(gameTime);
    }

    public double RevealRate {
      get { return _textDisplay.RevealRate; }
      set { _textDisplay.RevealRate = value; }
    }

    public void SetText(string text) {
      _isAllTextDisplayed = false;
      _lines = _textDisplay.SplitStringIntoLines(text).ToArray();
      SetCurrentLineGroup(0);
    }

    private void SetCurrentLineGroup(int lineGroup) {
      _currentLineGroup = lineGroup;
      if (_currentLineGroup*_maximumDisplayLines >= _lines.Count()) {
        _isAllTextDisplayed = true;
        OnAllTextDisplayed();
      }
      else {
        var textToDisplay = new StringBuilder();
        for (int i = 0; i < _maximumDisplayLines; i++) {
          int index = _currentLineGroup*_maximumDisplayLines + i;
          if (index < _lines.Count()) {
            if (i > 0) {
              textToDisplay.Append(" ");
            }
            textToDisplay.Append(_lines[index]);
          }
        }
        _textDisplay.SetTextToReveal(textToDisplay.ToString());
        _isAwaitingInput = false;
      }
    }

    private void AdvanceLineGroup() {
      SetCurrentLineGroup(_currentLineGroup + 1);
    }

    public void AdvanceText() {
      if (_isAwaitingInput) {
        AdvanceLineGroup();
      }
    }


    public override void Draw(SpriteBatch spriteBatch, Matrix parentTransformation) {
      var transformationMatrix = GetLocalTransformationMatrix() * parentTransformation;
      _borderBox.Draw(spriteBatch, transformationMatrix);
      _textDisplay.Draw(spriteBatch, transformationMatrix);
      if (_isAwaitingInput) {
        _cursor.Draw(spriteBatch, transformationMatrix);
      }
    }
  }
}
