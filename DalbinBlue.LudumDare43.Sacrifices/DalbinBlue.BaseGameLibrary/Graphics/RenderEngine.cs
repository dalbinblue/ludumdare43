﻿using System;
using System.Collections.Generic;
using Dalbinblue.BaseGameLibrary.Graphics.Camera;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dalbinblue.BaseGameLibrary.Graphics {
  public class RenderEngine : IEngineComponent {
    public const int MaximumSupportedLayers = 8;

    private readonly GraphicsDeviceManager _graphicsDeviceManager;
    private readonly GraphicsDevice _graphicsDevice;
    private SpriteBatch _spriteBatch;
    //private readonly List<IRenderableObject> _sceneElements = new List<IRenderableObject>();
    //private readonly List<IRenderableObject> _hudElements = new List<IRenderableObject>();
    private float _screenScaleFactor;

    private List<Layer> _layers = new List<Layer>(); 

    public ICamera Camera { get; set; }

    public RenderEngine(GraphicsDeviceManager graphicsDeviceManager, int targetWidth, int targetHeight, bool scaleToScreenSize) {
      _graphicsDeviceManager = graphicsDeviceManager;
      _graphicsDevice = graphicsDeviceManager.GraphicsDevice;
      _spriteBatch = new SpriteBatch(graphicsDeviceManager.GraphicsDevice);

      InitializeGraphicDevice(targetWidth, targetHeight, scaleToScreenSize);

      Camera = new StaticCamera();
    }

    private void InitializeGraphicDevice(int targetWidth, int targetHeight, bool scaleToScreenSize) {
      if (scaleToScreenSize)
      {
        var screenWidth = _graphicsDeviceManager.GraphicsDevice.Adapter.CurrentDisplayMode.Width - 40;
        var screenHeight = _graphicsDeviceManager.GraphicsDevice.Adapter.CurrentDisplayMode.Height - 40;

        _screenScaleFactor = Math.Min(screenWidth / targetWidth, screenHeight / targetHeight);
        if (_screenScaleFactor < 1) _screenScaleFactor = 1;
        if (_screenScaleFactor > 8) _screenScaleFactor = 8;
        _graphicsDeviceManager.PreferredBackBufferWidth = Convert.ToInt32(targetWidth * _screenScaleFactor);
        _graphicsDeviceManager.PreferredBackBufferHeight = Convert.ToInt32(targetHeight * _screenScaleFactor);
      }
      else
      {
        _screenScaleFactor = 1.0f;
        _graphicsDeviceManager.PreferredBackBufferWidth = targetWidth;
        _graphicsDeviceManager.PreferredBackBufferHeight = targetHeight;
      }

      _graphicsDeviceManager.GraphicsDevice.SamplerStates[0] = new SamplerState { Filter = TextureFilter.Point };
      _graphicsDeviceManager.ApplyChanges();           
    }


    public void Update(GameTime gameTime) {
      Camera.Update(gameTime);

      List<Layer> layers = new List<Layer>(_layers);
      foreach (var layer in layers) {
        layer.Update(gameTime);
      }

      //List<IRenderableObject> currentSceneElements = new List<IRenderableObject>(_sceneElements);

      //foreach (IRenderableObject renderableObject in currentSceneElements) {
      //  renderableObject.PostMovementUpdate(gameTime);
      //}

      //List<IRenderableObject> currentHudElements = new List<IRenderableObject>(_hudElements);

      //foreach (IRenderableObject renderableObject in currentHudElements) {
      //  renderableObject.PostMovementUpdate(gameTime);
      //}
    }

    public void Draw(GameTime gameTime) {
      _graphicsDevice.Clear(Color.Black);

      CurrentCameraTransformationMatrix = GetCameraTransformationMatrix();

      foreach (var layer in _layers) {
        _spriteBatch.Begin(SpriteSortMode.Immediate,
                           layer.DesiredBlendState ?? BlendState.AlphaBlend,
                           SamplerState.PointClamp,
                           DepthStencilState.Default,
                           RasterizerState.CullNone,
                           null,
                           layer.IsHudLayer ? GetHudTransformationMatrix() : CurrentCameraTransformationMatrix);

        layer.Draw(_spriteBatch, Matrix.Identity);

        _spriteBatch.End();
      }



      //foreach (IRenderableObject renderableObject in _sceneElements) {
      //  renderableObject.Draw(_spriteBatch, Matrix.Identity);
      //}

      //_spriteBatch.End();


      
      //foreach (IRenderableObject renderableObject in _hudElements) {
      //  renderableObject.Draw(_spriteBatch, Matrix.Identity);
      //}

      //_spriteBatch.End();

    }

    public static Matrix CurrentCameraTransformationMatrix { get; protected set; }

    private Matrix GetHudTransformationMatrix()
    {
      var viewport = _graphicsDevice.Viewport;

      return Matrix.CreateScale(new Vector3(_screenScaleFactor, _screenScaleFactor, 1));
    }

    protected Matrix GetCameraTransformationMatrix() {
      var viewport = _graphicsDevice.Viewport;

      return Matrix.CreateTranslation(new Vector3(-Camera.X, -Camera.Y, 0))
             * Matrix.CreateRotationZ(Camera.Rotation)
             * Matrix.CreateScale(new Vector3(Camera.Zoom * _screenScaleFactor, Camera.Zoom * _screenScaleFactor, 1))
             * Matrix.CreateTranslation(new Vector3(viewport.Width * 0.5f, viewport.Height * 0.5f, 0));
    }

    public void AddLayerToTop(Layer layer) {
      _layers.Add(layer);
    }

    public void AddLayerToBottom(Layer layer) {
      _layers.Insert(0, layer);
    }

    public void InsertLayer(int index, Layer layer) {
      _layers.Insert(index, layer);
    }

    public void RemoveLayer(Layer layer) {
      _layers.Remove(layer);
    }

    public void RemoveLayerat(int index) {
      _layers.RemoveAt(index);
    }

    public void ClearLayers() {
      _layers.Clear();
    }

    //public void AddSceneElement(IRenderableObject renderableObject) {
    //  _sceneElements.Add(renderableObject);
    //}

    //public void AddHudElement(IRenderableObject renderableObject)
    //{
    //  _hudElements.Add(renderableObject);
    //}

    //public void RemoveSceneElement(IRenderableObject renderableObject)
    //{
    //  _sceneElements.Remove(renderableObject);
    //}

    //public void RemoveHudElement(IRenderableObject renderableObject)
    //{
    //  _hudElements.Remove(renderableObject);
    //}

    //public void ClearScene()
    //{
    //  _sceneElements.Clear();
    //}

    //public void ClearHud()
    //{
    //  _hudElements.Clear();
    //}

    //public bool HasSceneElement(IRenderableObject sprite)
    //{
    //  return _sceneElements.Contains(sprite);
    //}
  }
}
