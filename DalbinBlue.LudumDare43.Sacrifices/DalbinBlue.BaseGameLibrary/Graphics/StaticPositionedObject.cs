﻿using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary.Graphics {
  internal class StaticPositionedObject : IPositionedObject {
    private readonly Vector2 _target;

    public StaticPositionedObject(Vector2 target) {
      _target = target;
    }

    public Vector2 Position {
      get { return _target; }
    }
  }
}