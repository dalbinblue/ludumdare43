namespace Dalbinblue.BaseGameLibrary.Graphics {
  public class TileGridTile {
    public FixedSizeTileset Tileset { get; set; }
    public int TileId { get; set; }
  }
}