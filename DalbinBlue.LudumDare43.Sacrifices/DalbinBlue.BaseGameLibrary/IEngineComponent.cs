﻿using Microsoft.Xna.Framework;

namespace Dalbinblue.BaseGameLibrary
{
    public interface IEngineComponent
    {
        void Update(GameTime gameTime);
    }
}