﻿using System;

namespace Dalbinblue.BaseGameLibrary.Input {
  public class ControllerButtonAlreadyExistsException : Exception {
    public ControllerButtonAlreadyExistsException(string buttonName)
      : base(string.Format("A mapping for name '{0}' already exists.", buttonName)) { }
  }
}