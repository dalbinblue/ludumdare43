﻿using System;

namespace Dalbinblue.BaseGameLibrary.Input {
  public class ControllerButtonNotDefinedException : Exception {
    public ControllerButtonNotDefinedException(string buttonName)
      : base(string.Format("Button '{0} is not defined", buttonName)) { }
  }
}