﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace Dalbinblue.BaseGameLibrary.Input {
  public class ControllerButtonSettings {
    public const double DefaultSecondsPerButtonPressRepeat = 0.5;
    public const double DefaultDoubleTapThreshold = 0.25;

    public List<Keys> MappedKeyboardKeys { get; private set; }
    public List<Buttons> MappedControllerButtons { get; private set; }
    public bool RepeatButtonPressOnHold { get; set; }
    public double SecondsPerButtonPressRepeat { get; set; }
    public double DoubleTapThreshold { get; set; }

    public ControllerButtonSettings() {
      MappedKeyboardKeys = new List<Keys>();
      MappedControllerButtons = new List<Buttons>();
    }
  }
}