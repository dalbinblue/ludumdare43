using System;
using Microsoft.Xna.Framework.Input;

namespace Dalbinblue.BaseGameLibrary.Input
{
  public abstract class ControllerStateBase
  {   
    [ControllerButton("Up", DefaultKey = Keys.Up, DefaultButton = Buttons.DPadUp)]
    public ControllerButtonState Up { get; protected set; }

    [ControllerButton("Down", DefaultKey = Keys.Down, DefaultButton = Buttons.DPadDown)]
    public ControllerButtonState Down { get; protected set; }

    [ControllerButton("Left", DefaultKey = Keys.Left, DefaultButton = Buttons.DPadLeft)]
    public ControllerButtonState Left { get; protected set; }

    [ControllerButton("Right", DefaultKey = Keys.Right, DefaultButton = Buttons.DPadRight)]
    public ControllerButtonState Right { get; protected set; }

    public CardinalDirection Direction
    {
      get
      {
        if (Up.IsDown) return CardinalDirection.Up;
        if (Down.IsDown) return CardinalDirection.Down;
        if (Left.IsDown) return CardinalDirection.Left;
        if (Right.IsDown) return CardinalDirection.Right;
        return CardinalDirection.None;
      }
    }

  }

  public enum CardinalDirection
  {
    None,
    Up,
    Down,
    Left,
    Right
  }

  public static class CardinalDirectionExtensions {
    public static CardinalDirection GetOppositeDirection(this CardinalDirection cardinalDirection) {
      switch (cardinalDirection) {
        case CardinalDirection.None:
          return CardinalDirection.None;
        case CardinalDirection.Up:
          return CardinalDirection.Down;
        case CardinalDirection.Down:
          return CardinalDirection.Up;
        case CardinalDirection.Left:
          return CardinalDirection.Right;
        case CardinalDirection.Right:
          return CardinalDirection.Left;
        default:
          throw new ArgumentOutOfRangeException("cardinalDirection");
      }
    }

      public static CardinalDirection GetClockwiseRotation(this CardinalDirection cardinalDirection)
      {
          switch (cardinalDirection)
          {
              case CardinalDirection.None:
                  return CardinalDirection.None;
              case CardinalDirection.Up:
                  return CardinalDirection.Right;
              case CardinalDirection.Down:
                  return CardinalDirection.Left;
              case CardinalDirection.Left:
                  return CardinalDirection.Up;
              case CardinalDirection.Right:
                  return CardinalDirection.Down;
              default:
                  throw new ArgumentOutOfRangeException(nameof(cardinalDirection), cardinalDirection, null);
          }
      }

      public static CardinalDirection GetCounterClockwiseRotation(this CardinalDirection cardinalDirection)
      {
          switch (cardinalDirection)
          {
              case CardinalDirection.None:
                  return CardinalDirection.None;
              case CardinalDirection.Up:
                  return CardinalDirection.Left;
              case CardinalDirection.Down:
                  return CardinalDirection.Right;
              case CardinalDirection.Left:
                  return CardinalDirection.Down;
              case CardinalDirection.Right:
                  return CardinalDirection.Up;
              default:
                  throw new ArgumentOutOfRangeException(nameof(cardinalDirection), cardinalDirection, null);
          }
      }

    }
}