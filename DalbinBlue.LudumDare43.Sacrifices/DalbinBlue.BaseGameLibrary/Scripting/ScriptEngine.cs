﻿using System.Collections.Generic;
using System.Linq;

namespace Dalbinblue.BaseGameLibrary.Scripting
{
  public delegate IEnumerator<ScriptState> Script();
  public delegate IEnumerator<ScriptState> Script<in T1>(T1 param1);
  public delegate IEnumerator<ScriptState> Script<in T1, in T2>(T1 param1, T2 param2);
  public delegate IEnumerator<ScriptState> Script<in T1, in T2, in T3>(T1 param1, T2 param2, T3 param3);
  public delegate IEnumerator<ScriptState> Script<in T1, in T2, in T3, in T4>(T1 param1, T2 param2, T3 param3, T4 param4);

  public class ScriptEngine
  {
    private readonly Stack<ScriptContainer> _scripts = new Stack<ScriptContainer>();

    public bool AreAnyScriptsExecuting { get { return _scripts.Any(); } }

    public bool ExecuteScript(Script script, ScriptState callerState)
    {
      return InitialExecuteScriptEnumerator(script(), callerState);
    }

    public bool ExecuteScript<T1>(Script<T1> script, ScriptState callerState, T1 param1)
    {
      return InitialExecuteScriptEnumerator(script(param1), callerState);
    }

    public bool ExecuteScript<T1, T2>(Script<T1, T2> script, ScriptState callerState, T1 param1, T2 param2)
    {
      return InitialExecuteScriptEnumerator(script(param1, param2), callerState);
    }

    public bool ExecuteScript<T1, T2, T3>(Script<T1, T2, T3> script, ScriptState callerState, T1 param1, T2 param2, T3 param3)
    {
      return InitialExecuteScriptEnumerator(script(param1, param2, param3), callerState);
    }

    public bool ExecuteScript<T1, T2, T3, T4>(Script<T1, T2, T3, T4> script, ScriptState callerState, T1 param1, T2 param2, T3 param3, T4 param4)
    {
      return InitialExecuteScriptEnumerator(script(param1, param2, param3, param4), callerState);
    }

    public bool ExecuteScript(Script script)
    {
      return InitialExecuteScriptEnumerator(script(), null);
    }

    public bool ExecuteScript<T1>(Script<T1> script, T1 param1)
    {
      return InitialExecuteScriptEnumerator(script(param1), null);
    }

    public bool ExecuteScript<T1, T2>(Script<T1, T2> script, T1 param1, T2 param2)
    {
      return InitialExecuteScriptEnumerator(script(param1, param2), null);
    }

    public bool ExecuteScript<T1, T2, T3>(Script<T1, T2, T3> script, T1 param1, T2 param2, T3 param3)
    {
      return InitialExecuteScriptEnumerator(script(param1, param2, param3), null);
    }

    public bool ExecuteScript<T1, T2, T3, T4>(Script<T1, T2, T3, T4> script, T1 param1, T2 param2, T3 param3, T4 param4)
    {
      return InitialExecuteScriptEnumerator(script(param1, param2, param3, param4), null);
    }

    private bool InitialExecuteScriptEnumerator(IEnumerator<ScriptState> enumerator, ScriptState callerState)
    {
      enumerator.MoveNext();
      var state = enumerator.Current;

      if (state == null || state.IsComplete)
      {
        if (callerState != null)
        {
          callerState.IsComplete = true;
          if (state != null)
          {
            callerState.ReturnValue = state.ReturnValue;
          }
        }
        return true;
      }

      var container = new ScriptContainer
      {
        ScriptEnumerator = enumerator,
        CallerState = callerState,
        WaitingState = state
      };

      _scripts.Push(container);
      return false;
    }

    public void Update()
    {
      bool topScriptCompleted;
      do
      {
        if (!AreAnyScriptsExecuting) break;

        ScriptContainer container = _scripts.Peek();

        if (container.WaitingState != null && !container.WaitingState.IsComplete)
        {
          break;
        }

        container.ScriptEnumerator.MoveNext();
        ScriptState state = container.ScriptEnumerator.Current;

        if (state == null || state.IsComplete)
        {
          if (container.CallerState != null)
          {
            container.CallerState.IsComplete = true;
            if (state != null)
            {
              container.CallerState.ReturnValue = state.ReturnValue;
            }
          }
          _scripts.Pop();
          topScriptCompleted = true;
        }
        else
        {
          container.WaitingState = state;
          topScriptCompleted = false;
        }
      } while (topScriptCompleted);
    }

    private class ScriptContainer
    {
      public ScriptState CallerState { get; set; }
      public ScriptState WaitingState { get; set; }
      public IEnumerator<ScriptState> ScriptEnumerator { get; set; }
    }

      public void Clear() {
          _scripts.Clear();
      }
  }
}
