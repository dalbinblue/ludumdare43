﻿namespace DalbinBlue.LudumDare43.Sacrifices
{
    public enum ActorType
    {
        SpawnPoint,
        EndGoal,
        Crate,
        SteelBlock,
        ButtonRed,
        ButtonYellow,
        ButtonGreen,
        ButtonBlue,
        DoorClosedRed,
        DoorClosedYellow,
        DoorClosedGreen,
        DoorClosedBlue,
        DoorOpenRed,
        DoorOpenYellow,
        DoorOpenGreen,
        DoorOpenBlue,
        Bomb,
        BreakableWall,
        ElectrifiedFloor,
        ConveyorRight,
        ConveyorLeft
    }
}