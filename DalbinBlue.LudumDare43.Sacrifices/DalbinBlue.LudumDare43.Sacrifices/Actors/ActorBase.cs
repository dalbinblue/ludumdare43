﻿using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public abstract class ActorBase
    {
        protected AnimatedSprite Sprite { get; set; }

        protected GameEngine GameEngine { get; set; }

        protected ActorBase(GameEngine gameEngine)
        {
            GameEngine = gameEngine;
        }

        public abstract void Update(GameTime gameTime);

        public virtual void RemoveGraphicsFromScene()
        {
            GameEngine.ActorLayer.Remove(Sprite);
        }

        public virtual void AddGraphicsToScene()
        {
            GameEngine.ActorLayer.Add(Sprite);
        }

        public virtual Rectangle GetCollisionBounds()
        {
            return Sprite.GetCollisionBounds();
        }

        public virtual Vector2 Position
        {
            get { return Sprite.Position; }
            set { Sprite.Position = value; }
        }

        public virtual void HandleCollisionWithPlayer()
        {
            // DO NOTHING
        }

        public virtual bool BlocksMovement => false;

        public virtual bool CanBePushed => false;

        public virtual void HandleCollisionWithActor(ActorBase otherActor)
        {
            // DO NOTHING
        }
    }
}