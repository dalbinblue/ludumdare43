﻿using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class Bomb : MovingActorBase
    {
        public Bomb(GameEngine gameEngine) : base(gameEngine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(GameEngine.ActorSpriteSheet, "Bomb", GameEngine.Content);
            Sprite.SetAnimation("Default");
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsMoving)
            {
                TryFall();
            }
        }

        public override bool BlocksMovement => true;
        public override bool CanBePushed => true;
    }
}