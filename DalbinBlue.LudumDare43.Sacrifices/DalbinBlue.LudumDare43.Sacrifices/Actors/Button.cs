﻿using System;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class Button : ActorBase
    {
        public ButtonColor Color { get; }

        public Button(GameEngine gameEngine, ButtonColor color) : base(gameEngine)
        {
            Color = color;
            Sprite = AnimatedSprite.FromSpriteSheet(GameEngine.ActorSpriteSheet, $"{Color}Button", GameEngine.Content);
            Sprite.SetAnimation("Up");
        }

        public bool IsPressed { get; private set; } = false;
        
        public override void Update(GameTime gameTime)
        {
            var previousState = IsPressed;
            IsPressed = false;
            var collisionBounds = GetCollisionBounds();
            if (GameEngine.Player.GetCollisionBounds().Intersects(collisionBounds) || 
                GameEngine.Actors.Any(a => a.BlocksMovement && a.GetCollisionBounds().Intersects(collisionBounds)))
            {
                IsPressed = true;
            }

            if (previousState != IsPressed)
            {
                GameEngine.SoundManager.PlaySoundEffect("switch");
                GameEngine.UpdateButtonState(Color);
            }

            Sprite.SetAnimation(IsPressed ? "Down" : "Up");
        }
    }
}