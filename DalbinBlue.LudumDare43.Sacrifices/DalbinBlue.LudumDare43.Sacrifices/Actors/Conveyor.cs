﻿using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class Conveyor : ActorBase
    {
        private readonly CardinalDirection _direction;

        public Conveyor(GameEngine gameEngine, CardinalDirection direction) : base(gameEngine)
        {
            _direction = direction;
            Sprite = AnimatedSprite.FromSpriteSheet(GameEngine.ActorSpriteSheet, "Conveyor", GameEngine.Content);
            Sprite.SetAnimation($"Spin{direction}");
        }

        public override void Update(GameTime gameTime)
        {
            // DO NOTHING
        }

        public override void HandleCollisionWithPlayer()
        {
            if (GameEngine.Player.IsMoving) return;
            if (_direction == CardinalDirection.Left)
            {
                GameEngine.Player.TryWalkingLeft();
            }
            else 
            {
                GameEngine.Player.TryWalkingRight();
            }
        }

        public override void HandleCollisionWithActor(ActorBase otherActor)
        {
            if (!(otherActor is MovingActorBase movingActor)) return;
            if (movingActor.IsMoving) return;
            if (!movingActor.CanBePushed && !(movingActor is SteelBlock)) return;
            if (!movingActor.CanMove(_direction)) return;
            movingActor.Move(_direction);
        }
    }
}