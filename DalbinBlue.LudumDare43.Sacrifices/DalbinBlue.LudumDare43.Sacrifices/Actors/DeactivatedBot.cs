﻿using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class DeactivatedBot : MovingActorBase
    {
        public DeactivatedBot(GameEngine gameEngine) : base(gameEngine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(GameEngine.PlayerSpriteSheet, "Player", GameEngine.Content);
            Sprite.SetAnimation("Deactivated");
            GameEngine.SoundManager.PlaySoundEffect("deactivate");
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsMoving)
            {
                TryFall();
            }
        }

        public override bool BlocksMovement => true;
        public override bool CanBePushed => true;
    }
}