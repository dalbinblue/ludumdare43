﻿using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class Door : ActorBase
    {
        private readonly bool _isOpenWhenButtonUp;
        private readonly ButtonColor _color;
        private bool _blocksMovement;

        public Door(GameEngine gameEngine, ButtonColor color, bool isOpenWhenButtonUp) : base(gameEngine)
        {
            _isOpenWhenButtonUp = isOpenWhenButtonUp;
            _color = color;
            Sprite = AnimatedSprite.FromSpriteSheet(GameEngine.ActorSpriteSheet, $"{_color}Door", GameEngine.Content);
            Sprite.SetAnimation(isOpenWhenButtonUp ? "Open" : "Closed");
            Sprite.SpriteAnimationEnded += SpriteOnSpriteAnimationEnded;
            _blocksMovement = !isOpenWhenButtonUp;

        }

        private void SpriteOnSpriteAnimationEnded(object sender, SpriteAnimageEndedEventArgs args)
        {
            if (args.AnimationName == "Closing")
            {
                Sprite.SetAnimation("Closed");
                _blocksMovement = true;
            }
            else if (args.AnimationName == "Opening")
            {
                Sprite.SetAnimation("Open");
                _blocksMovement = false;
            }
        }

        public override void Update(GameTime gameTime)
        {
            var animation = Sprite.CurrentAnimationName;
            if (GameEngine.ButtonStates[(int) _color]) // Down
            {
                if (_isOpenWhenButtonUp && animation != "Closed" && animation != "Closing")
                {
                    Sprite.SetAnimation("Closing");
                    _blocksMovement = true;
                }
                else if (!_isOpenWhenButtonUp && animation != "Open" && animation != "Opening")
                {
                    Sprite.SetAnimation("Opening");
                }
            }
            else
            {
                if (!_isOpenWhenButtonUp && animation != "Closed" && animation != "Closing")
                {
                    Sprite.SetAnimation("Closing");
                    _blocksMovement = true;
                }
                else if (_isOpenWhenButtonUp && animation != "Open" && animation != "Opening")
                {
                    Sprite.SetAnimation("Opening");
                }
            }
        }

        public override bool BlocksMovement => _blocksMovement;
    }
}