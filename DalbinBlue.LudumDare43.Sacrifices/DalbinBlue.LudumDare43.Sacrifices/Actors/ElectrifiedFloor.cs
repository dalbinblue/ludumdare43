﻿using System;
using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class ElectrifiedFloor : ActorBase
    {
        public ElectrifiedFloor(GameEngine gameEngine) : base(gameEngine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(GameEngine.ActorSpriteSheet, "ElectrifiedFloor", GameEngine.Content);
            Sprite.SetAnimation("Default");
        }

        public override void Update(GameTime gameTime)
        {
            // DO NOTHING
        }

        public override void HandleCollisionWithPlayer()
        {
            if (Math.Abs(GameEngine.Player.Position.ToPoint().Y - Position.ToPoint().Y) > 1) return;
            if (Math.Abs(GameEngine.Player.Position.ToPoint().X - Position.ToPoint().X) > 1) return;
            var bot = new ExplosiveBot(GameEngine, true);
            bot.Position = Position;
            GameEngine.AddActor(bot);
            GameEngine.SpawnPlayer();
        }

        public override void HandleCollisionWithActor(ActorBase otherActor)
        {
            switch (otherActor)
            {
                case Bomb bomb:
                    if (bomb.IsMoving) return;
                    var explosion = new Explosion(GameEngine);
                    explosion.Position = bomb.Position;
                    GameEngine.RemoveActor(bomb);
                    GameEngine.AddActor(explosion);
                    break;
                case ExplosiveBot bot:
                    if (bot.IsMoving) return;
                    var explosion1 = new Explosion(GameEngine);
                    explosion1.Position = bot.Position;
                    GameEngine.RemoveActor(bot);
                    GameEngine.AddActor(explosion1);
                    break;
                case DeactivatedBot bot:
                    if (bot.IsMoving) return;
                    var explosion2 = new Explosion(GameEngine);
                    explosion2.Position = bot.Position;
                    GameEngine.RemoveActor(bot);
                    GameEngine.AddActor(explosion2);
                    break;
            }
        }

        public override void AddGraphicsToScene()
        {
            GameEngine.BackActorLayer.Add(Sprite);
        }

        public override void RemoveGraphicsFromScene()
        {
            GameEngine.BackActorLayer.Remove(Sprite);
        }
    }
}