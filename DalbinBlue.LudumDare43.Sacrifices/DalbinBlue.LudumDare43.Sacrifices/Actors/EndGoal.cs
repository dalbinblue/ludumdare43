﻿using Dalbinblue.BaseGameLibrary;
using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class EndGoal : MovingActorBase
    {
        public EndGoal(GameEngine gameEngine) : base(gameEngine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(GameEngine.ActorSpriteSheet, "EndGoal", GameEngine.Content);
            Sprite.SetAnimation("Default");
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsMoving)
            {
                TryFall();
            }
        }

        public override void HandleCollisionWithPlayer()
        {
            if (IsMoving) return;
            if (GameEngine.Player.Position.ToPoint() != Position.ToPoint()) return;
            GameEngine.CompleteLevel();
        }

        public override void AddGraphicsToScene()
        {
            GameEngine.BackActorLayer.Add(Sprite);
        }

        public override void RemoveGraphicsFromScene()
        {
            GameEngine.BackActorLayer.Remove(Sprite);
        }

        protected override bool CheckCanFall()
        {
            var collisionBounds = GetCollisionBounds().Shift(new Point(0, 16));
            collisionBounds = new Rectangle(
                collisionBounds.X,
                collisionBounds.Y + collisionBounds.Height - 16,
                collisionBounds.Width,
                8);

            if (GameEngine.FallingCollision.TestNonMovingCollision(collisionBounds)) return false;
            return !CollidesWithBlockingActors(collisionBounds);
        }

    }
}