﻿using System.Linq;
using System.Runtime.Serialization;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class Explosion : ActorBase
    {
        public Explosion(GameEngine gameEngine) : base(gameEngine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(GameEngine.PlayerSpriteSheet, "Explosion", GameEngine.Content);
            Sprite.SetAnimation("Start");
            Sprite.SpriteAnimationEnded += SpriteOnSpriteAnimationEnded;
            GameEngine.SoundManager.PlaySoundEffect("explosion");
        }

        private void SpriteOnSpriteAnimationEnded(object sender, SpriteAnimageEndedEventArgs args)
        {
            GameEngine.RemoveActor(this);
        }

        public override void Update(GameTime gameTime)
        {
            //TODO Implement collision response
        }

        public override void HandleCollisionWithPlayer()
        {
            if (Sprite.CurrentAnimationName != "Start") return;

            if (GameEngine.Player.Position.ToPoint() == GameEngine.PlayerSpawnPoint.ToPoint()) return;
            var bot = new ExplosiveBot(GameEngine, true);
            bot.Position = GameEngine.Player.Position;
            GameEngine.AddActor(bot);
            GameEngine.SpawnPlayer();
        }

        public override void HandleCollisionWithActor(ActorBase otherActor)
        {
            if (Sprite.CurrentAnimationName != "Start") return;

            switch (otherActor)
            {
                case DeactivatedBot deactivatedBot:
                    var bot = new ExplosiveBot(GameEngine, true);
                    bot.Position = deactivatedBot.Position;
                    GameEngine.AddActor(bot);
                    GameEngine.RemoveActor(deactivatedBot);
                    break;
                case Crate crate:
                    GameEngine.RemoveActor(crate);
                    break;
                case SteelBlock steelBlock:
                    if (steelBlock.Position.ToPoint().X > Position.ToPoint().X)
                    {
                        steelBlock.Fly(CardinalDirection.Right);
                    }
                    else if (steelBlock.Position.ToPoint().X < Position.ToPoint().X)
                    {
                        steelBlock.Fly(CardinalDirection.Left);
                    }

                    break;
                case Bomb bomb:
                    if (GameEngine.Actors.OfType<Explosion>().Any(e => e.Position.ToPoint() == bomb.Position.ToPoint())) return;

                    var explosion = new Explosion(GameEngine);
                    explosion.Position = bomb.Position;
                    GameEngine.RemoveActor(bomb);
                    GameEngine.AddActor(explosion);
                    break;
                case BreakableWall wall:
                    GameEngine.RemoveActor(wall);
                    break;
            }
        }
    }
}