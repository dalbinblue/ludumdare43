﻿using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class ExplosiveBot : MovingActorBase
    {
        public ExplosiveBot(GameEngine gameEngine, bool isFastCountdown) : base(gameEngine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(GameEngine.PlayerSpriteSheet, "Player", GameEngine.Content);
            Sprite.SetAnimation(isFastCountdown ? "FastCountdown" : "Countdown");
            Sprite.SpriteAnimationEnded += SpriteOnSpriteAnimationEnded;
        }

        private void SpriteOnSpriteAnimationEnded(object sender, SpriteAnimageEndedEventArgs args)
        {
            GameEngine.RemoveActor(this);

            var explosion = new Explosion(GameEngine);
            explosion.Position = Position;
            GameEngine.AddActor(explosion);
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsMoving && Sprite.CurrentAnimationName == "Countdown")
            {
                TryFall();
            }
        }
        public override bool BlocksMovement => true;
        public override bool CanBePushed => true;
    }
}