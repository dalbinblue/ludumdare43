﻿using System;
using System.Linq;
using Dalbinblue.BaseGameLibrary;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public abstract class MovingActorBase : ActorBase
    {
        public float TimeToMoveSpace { get; set; }

        public MovingActorBase(GameEngine gameEngine) : base(gameEngine)
        {
            TimeToMoveSpace = 0.20f;
        }

        public bool CanMove(CardinalDirection direction)
        {
            switch (direction)
            {
                case CardinalDirection.None:
                    return true;
                //case CardinalDirection.Up:
                //    return CheckForLadderUp();
                case CardinalDirection.Down:
                    return CheckCanFall();
                case CardinalDirection.Left:
                    return CheckCanWalk(-16);
                case CardinalDirection.Right:
                    return CheckCanWalk(16);
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        public bool TryFall()
        {
            if (!IsMoving && CanMove(CardinalDirection.Down))
            {
                Move(CardinalDirection.Down);
                return true;
            }

            return false;
        }

        private bool CheckCanWalk(int xShift)
        {
            var collisionBounds = GetCollisionBounds().Shift(new Point(xShift, 0));
            if (GameEngine.HorizontalCollision.TestNonMovingCollision(collisionBounds)) return false;
            return !CollidesWithBlockingActors(collisionBounds);
        }

        protected bool CollidesWithBlockingActors(Rectangle collisionBounds)
        {
            var blockingActors = GameEngine.Actors.Where(a => a != this && a.BlocksMovement);
            return blockingActors.Any(blockingActor => blockingActor.GetCollisionBounds().Intersects(collisionBounds));
        }

        protected virtual bool CheckCanFall()
        {
            var collisionBounds = GetCollisionBounds().Shift(new Point(0, 16));
            collisionBounds = new Rectangle(
                collisionBounds.X,
                collisionBounds.Y + collisionBounds.Height - 16,
                collisionBounds.Width,
                8);

            if (GameEngine.FallingCollision.TestNonMovingCollision(collisionBounds)) return false;
            if (collisionBounds.Intersects(GameEngine.Player.GetCollisionBounds())) return false;
            return !CollidesWithBlockingActors(collisionBounds);
        }

        public bool CheckForLadderUp()
        {
            if (!GameEngine.Ladders.IsObstacleAtTilePosition(new Point((int)(Sprite.Position.X / 16), (int)(Sprite.Position.Y / 16))))
            {
                return false;
            }
            var collisionBounds = GetCollisionBounds().Shift(new Point(0, -16));
            if (GameEngine.HorizontalCollision.TestNonMovingCollision(collisionBounds)) return false;
            return !CollidesWithBlockingActors(collisionBounds);
        }

        public bool CheckForLadderDown()
        {
            if (!GameEngine.Ladders.IsObstacleAtTilePosition(new Point((int)(Sprite.Position.X / 16), (int)(Sprite.Position.Y / 16 + 1))))
            {
                return false;
            }
            var collisionBounds = GetCollisionBounds().Shift(new Point(0, 16));
            if (GameEngine.HorizontalCollision.TestNonMovingCollision(collisionBounds)) return false;
            return !CollidesWithBlockingActors(collisionBounds);
        }

        public void Move(CardinalDirection direction)
        {
            Vector2 shiftAmount;

            switch (direction)
            {
                case CardinalDirection.None:
                    return;
                case CardinalDirection.Up:
                    shiftAmount = new Vector2(0, -16);
                    break;
                case CardinalDirection.Down:
                    shiftAmount = new Vector2(0, 16);
                    break;
                case CardinalDirection.Left:
                    shiftAmount = new Vector2(-16, 0);
                    break;
                case CardinalDirection.Right:
                    shiftAmount = new Vector2(16, 0);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }

            Sprite.TweeningAnimationComplete += HandleAnimationComplete;
            Sprite.AnimatePosition(Position.Shift(shiftAmount), TimeToMoveSpace);
            IsMoving = true;
        }

        private void HandleAnimationComplete(object sender, TweeningAnimationEventHandlerArgs args)
        {
            IsMoving = false;
            Sprite.TweeningAnimationComplete -= HandleAnimationComplete;
        }

        public virtual bool IsMoving { get; private set; }
    }
}