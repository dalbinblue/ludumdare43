﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dalbinblue.BaseGameLibrary;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class Player : MovingActorBase
    {
        public Player(GameEngine gameEngine) : base(gameEngine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(gameEngine.PlayerSpriteSheet, "Player", GameEngine.Content);

        }

        public override void AddGraphicsToScene()
        {
            GameEngine.PlayerLayer.Add(Sprite);
        }

        public override void RemoveGraphicsFromScene()
        {
            GameEngine.PlayerLayer.Remove(Sprite);
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsMoving)
            {
                Sprite.SetAnimation("Stand");
                if (TryFall())
                {
                    Sprite.SetAnimation("Stand");
                }
            }
        }

        public void TryWalkingLeft()
        {
            if (!IsMoving)
            {
                if (CanMove(CardinalDirection.Left) && !CanMove(CardinalDirection.Down))
                {
                    Move(CardinalDirection.Left);
                    Sprite.SetAnimation("Walk");
                    Sprite.FlipHorizontally = true;
                    return;
                }
                TryPushLeft();
            }
        }

        public void TryWalkingRight()
        {
            if (!IsMoving)
            {
                if (CanMove(CardinalDirection.Right) && !CanMove(CardinalDirection.Down))
                {
                    Move(CardinalDirection.Right);
                    Sprite.SetAnimation("Walk");
                    Sprite.FlipHorizontally = false;
                    return;
                }
                TryPushRight();
            }
        }

        public void TryClimbUpLadder()
        {
            if (!IsMoving && CheckForLadderUp())
            {
                Move(CardinalDirection.Up);
                Sprite.SetAnimation("ClimbUp");
            }
        }

        public void TryClimbDownLadder()
        {
            if (!IsMoving && CheckForLadderDown())
            {
                Move(CardinalDirection.Down);
                Sprite.SetAnimation("ClimbDown");
            }
        }

        public void TryDeactivate()
        {
            if (!IsMoving)
            {
                var bot = new DeactivatedBot(GameEngine);
                bot.Position = Position;
                GameEngine.AddActor(bot);
                GameEngine.SpawnPlayer();
            }
        }

        public void TryExplode()
        {
            if (!IsMoving)
            {
                var bot = new ExplosiveBot(GameEngine, false);
                bot.Position = Position;
                GameEngine.AddActor(bot);
                GameEngine.SpawnPlayer();
            }
        }

        public void TryPushLeft()
        {
            var collisionBounds = GetCollisionBounds().Shift(new Point(-16, 0));
            if (GameEngine.HorizontalCollision.TestNonMovingCollision(collisionBounds)) return;
            var actor = GameEngine.Actors.OfType<MovingActorBase>().FirstOrDefault(b => b.GetCollisionBounds().Intersects(collisionBounds) && b.BlocksMovement);

            if (actor == null) return;

            var actorAbove = GameEngine.Actors.OfType<MovingActorBase>().FirstOrDefault(b => b.GetCollisionBounds().Intersects(actor.GetCollisionBounds().Shift(new Point(0, -16))) && b.BlocksMovement && b.CanBePushed);

            Sprite.FlipHorizontally = true;
            if (actor.IsMoving || !actor.CanBePushed || actor.CanMove(CardinalDirection.Down) ||
                !actor.CanMove(CardinalDirection.Left) || actorAbove != null)
            {
                Sprite.SetAnimation("NoPush");
                return;
            }
            actor.Move(CardinalDirection.Left);
            Move(CardinalDirection.Left);
            Sprite.SetAnimation("Push");
        }

        public void TryPushRight()
        {
            var collisionBounds = GetCollisionBounds().Shift(new Point(16, 0));
            if (GameEngine.HorizontalCollision.TestNonMovingCollision(collisionBounds)) return;
            var actor = GameEngine.Actors.OfType<MovingActorBase>().FirstOrDefault(b => b.GetCollisionBounds().Intersects(collisionBounds) && b.BlocksMovement);

            if (actor == null) return;

            var actorAbove = GameEngine.Actors.OfType<MovingActorBase>().FirstOrDefault(b => b.GetCollisionBounds().Intersects(actor.GetCollisionBounds().Shift(new Point(0,-16))) && b.BlocksMovement && b.CanBePushed);

            Sprite.FlipHorizontally = false;
            if (actor.IsMoving || !actor.CanBePushed || actor.CanMove(CardinalDirection.Down) ||
                !actor.CanMove(CardinalDirection.Right) || actorAbove != null)
            {
                Sprite.SetAnimation("NoPush");
                return;
            }
            actor.Move(CardinalDirection.Right);
            Move(CardinalDirection.Right);
            Sprite.SetAnimation("Push");
        }

        public IPositionedObject GetAttachedPositionedObject()
        {
            return Sprite;
        }

        public override bool BlocksMovement => true;
    }
}
