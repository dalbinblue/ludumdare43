﻿using System.Linq;
using Dalbinblue.BaseGameLibrary.Graphics;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class PlayerSpawnPoint : MovingActorBase
    {
        public PlayerSpawnPoint(GameEngine gameEngine) : base(gameEngine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(GameEngine.ActorSpriteSheet, "SpawnPoint", GameEngine.Content);
            Sprite.SetAnimation("Default");
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsMoving)
            {
                TryFall();
            }
            GameEngine.PlayerSpawnPoint = Position;
        }

        public override void AddGraphicsToScene()
        {
            GameEngine.BackActorLayer.Add(Sprite);
        }

        public override void RemoveGraphicsFromScene()
        {
            GameEngine.BackActorLayer.Remove(Sprite);
        }

    }
}