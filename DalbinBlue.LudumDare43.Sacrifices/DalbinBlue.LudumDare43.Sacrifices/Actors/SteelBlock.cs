﻿using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework;

namespace DalbinBlue.LudumDare43.Sacrifices.Actors
{
    public class SteelBlock : MovingActorBase
    {
        public SteelBlock(GameEngine gameEngine) : base(gameEngine)
        {
            Sprite = AnimatedSprite.FromSpriteSheet(GameEngine.ActorSpriteSheet, "SteelBlock", GameEngine.Content);
            Sprite.SetAnimation("Default");
        }

        public override void Update(GameTime gameTime)
        {
            if (!IsMoving)
            {
                if (_isFlying)
                {
                    if (CanMove(_flyDirection))
                    {
                        Move(_flyDirection);
                    }
                    else
                    {
                        _isFlying = false;
                    }
                }
                else
                {
                    TryFall();
                }
            }
        }

        public override bool BlocksMovement => true;
        public override bool CanBePushed => false;

        private bool _isFlying;
        private CardinalDirection _flyDirection;

        public void Fly(CardinalDirection direction)
        {
            if (CanMove(direction))
            {
                _flyDirection = direction;
                _isFlying = true;
            }
        }
    }
}