﻿using Dalbinblue.BaseGameLibrary.Input;
using Microsoft.Xna.Framework.Input;

namespace DalbinBlue.LudumDare43.Sacrifices
{
    public class ControllerState : ControllerStateBase
    {
        [ControllerButton("Reset", DefaultButton = Buttons.Y, DefaultKey = Keys.R)]
        public ControllerButtonState Reset { get; set; }

        [ControllerButton("Pause", DefaultButton = Buttons.Start, DefaultKey = Keys.Enter)]
        public ControllerButtonState Pause { get; set; }

        [ControllerButton("Deactivate", DefaultButton = Buttons.A, DefaultKey = Keys.LeftControl)]
        public ControllerButtonState Deactivate { get; set; }

        [ControllerButton("Explode", DefaultButton = Buttons.X, DefaultKey = Keys.Space)]
        public ControllerButtonState Explode { get; set; }

        [ControllerButton("Quit", DefaultButton = Buttons.Back, DefaultKey = Keys.Escape)]
        public ControllerButtonState Quit { get; set; }
    }
}