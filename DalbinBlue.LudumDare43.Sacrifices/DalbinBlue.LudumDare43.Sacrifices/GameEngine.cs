﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dalbinblue.BaseGameLibrary.Audio;
using Dalbinblue.BaseGameLibrary.Data.Map;
using Dalbinblue.BaseGameLibrary.Data.Sprite;
using Dalbinblue.BaseGameLibrary.Graphics;
using Dalbinblue.BaseGameLibrary.Graphics.Camera;
using Dalbinblue.BaseGameLibrary.Graphics.Hud;
using Dalbinblue.BaseGameLibrary.Input;
using Dalbinblue.BaseGameLibrary.Physics;
using DalbinBlue.LudumDare43.Sacrifices.Actors;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DalbinBlue.LudumDare43.Sacrifices
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameEngine : Game
    {
        // Graphics
        GraphicsDeviceManager _graphics;
        public RenderEngine RenderEngine { get; private set; }
        public TransitionCamera Camera { get; private set; }

        // Graphic Assets
        public SpriteSheet PlayerSpriteSheet { get; set; }
        public SpriteSheet ActorSpriteSheet { get; set; }
        public SpriteFont Font { get; set; }


        // Controls
        public ControllerManager<ControllerState> ControllerManager { get; private set; }

        // Layer
        public Layer BackgroundLayer { get; private set; }
        public Layer BackActorLayer { get; private set; }
        public Layer ActorLayer { get; private set; }
        public Layer PlayerLayer { get; private set; }
        public Layer HudLayer { get; private set; }

        // Sound
        public SoundManager SoundManager { get; private set; }

        // Level elements
        public TileMap Map { get; set; }
        public BasicTileCollisionMap HorizontalCollision { get; private set; }
        public BasicTileCollisionMap FallingCollision { get; private set; }
        public BasicTileCollisionMap Ladders { get; private set; }
        public List<ActorBase> Actors { get; private set; }
        public Player Player { get; private set; }
        public Vector2 PlayerSpawnPoint { get; set; }
        public int PlayerBotsLeft { get; set; }
        public int CurrentLevelNumber { get; set; }
        public bool[] ButtonStates { get; set; }

        public GameEngine()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            ControllerManager = new ControllerManager<ControllerState>();
            LoadGraphics();
            LoadSound();
            InitializeGraphics();
            InitializeHud();
            LoadLevel(0);
        }

        private void LoadSound()
        {
            SoundManager = new SoundManager(Content);
            SoundManager.RegisterMusic("theme","theme");

            SoundManager.RegisterSoundEffect("explosion", "explosion");
            SoundManager.RegisterSoundEffect("deactivate", "deactivate");
            SoundManager.RegisterSoundEffect("switch", "switch");

            SoundManager.PlayMusic("theme");
        }

        private RevealingTextDisplay HelpText { get; set; }
        private TextDisplay LevelText { get; set; }
        private TextDisplay BotCountText { get; set; }

        private void InitializeHud()
        {
            HelpText = new RevealingTextDisplay(Font, 320);
            HelpText.RevealRate = 30f;
            HelpText.Position = new Vector2(0, 240-32);
            HudLayer.Add(HelpText);

            LevelText = new TextDisplay(Font, 320);
            LevelText.Position = new Vector2(0, 0);
            HudLayer.Add(LevelText);

            BotCountText = new TextDisplay(Font, 320);
            BotCountText.Position = new Vector2(160, 0);
            HudLayer.Add(BotCountText);
        }

        private void LoadGraphics()
        {
            PlayerSpriteSheet = Content.Load<SpriteSheet>("playersheet");
            ActorSpriteSheet = Content.Load<SpriteSheet>("actorssheet");
            Font = Content.Load<SpriteFont>("font");
        }

        private void LoadLevel(int levelNumber)
        {
            CurrentLevelNumber = levelNumber;
            var levelSettings = LevelSettings.Levels[levelNumber];
            ButtonStates = new[] {false, false, false, false};

            BackgroundLayer.Clear();
            ActorLayer.Clear();
            BackActorLayer.Clear();
            PlayerLayer.Clear();

            // Load level
            Map = Content.Load<TileMap>($"Level{levelNumber}");
            HorizontalCollision = BasicTileCollisionMap.FromTileMapLayer(Map, Map.GetLayerIndexByName("Collision"));
            Ladders = BasicTileCollisionMap.FromTileMapLayer(Map, Map.GetLayerIndexByName("Ladders"));
            FallingCollision = HorizontalCollision.CombineWith(Ladders);

            // Set background layer
            var background = TileGrid.FromTileMapLayer(Map, Map.GetLayerIndexByName("Background"), Content);
            BackgroundLayer.Add(background);

            // Load actors
            LoadActors();

            // Initialize player
            PlayerBotsLeft = levelSettings.NumberOfBotsAvailable;
            SpawnPlayer();

            Camera.SceneHeight = Map.HeightInTiles * Map.TileHeight;
            Camera.SceneWidth = Map.WidthInTiles * Map.TileWidth;
            Camera.ClampPositionToScene = true;

            HelpText.SetTextToReveal(levelSettings.HelpText);
        }

        private void LoadActors()
        {
            Actors = new List<ActorBase>();
            var actorLayer = Map.ObjectGroups.First(og => og.Name == "Actors");

            foreach (var actorDefinition in actorLayer.Objects)
            {
                var tileIndex = actorDefinition.TileId;
                var matchingTileSet = Map.Tilesets
                    .Where(ts => ts.FirstTileId <= tileIndex)
                    .OrderByDescending(ts => ts.FirstTileId)
                    .First();

                var actorId = tileIndex - matchingTileSet.FirstTileId;
                var actorPosition = new Vector2(actorDefinition.X+8, actorDefinition.Y - 1);
                ActorType actorType = (ActorType)actorId;

                switch (actorType)
                {
                    case ActorType.SpawnPoint:
                        CreateSpawnPoint(actorPosition);
                        break;
                    case ActorType.EndGoal:
                        CreateEndGoal(actorPosition);
                        break;
                    case ActorType.Crate:
                        CreateCrate(actorPosition);
                        break;
                    case ActorType.SteelBlock:
                        CreateSteelBlock(actorPosition);
                        break;
                    case ActorType.Bomb:
                        CreateBomb(actorPosition);
                        break;
                    case ActorType.BreakableWall:
                        CreateBreakableWall(actorPosition);
                        break;
                    case ActorType.ElectrifiedFloor:
                        CreateElectrifiedFloor(actorPosition);
                        break;
                    case ActorType.ButtonRed:
                        CreateButton(ButtonColor.Red, actorPosition);
                        break;
                    case ActorType.ButtonYellow:
                        CreateButton(ButtonColor.Yellow, actorPosition);
                        break;
                    case ActorType.ButtonGreen:
                        CreateButton(ButtonColor.Green, actorPosition);
                        break;
                    case ActorType.ButtonBlue:
                        CreateButton(ButtonColor.Blue, actorPosition);
                        break;
                    case ActorType.DoorClosedRed:
                        CreateDoor(ButtonColor.Red, false, actorPosition);
                        break;
                    case ActorType.DoorClosedYellow:
                        CreateDoor(ButtonColor.Yellow, false, actorPosition);
                        break;
                    case ActorType.DoorClosedGreen:
                        CreateDoor(ButtonColor.Green, false, actorPosition);
                        break;
                    case ActorType.DoorClosedBlue:
                        CreateDoor(ButtonColor.Blue, false, actorPosition);
                        break;
                    case ActorType.DoorOpenRed:
                        CreateDoor(ButtonColor.Red, true, actorPosition);
                        break;
                    case ActorType.DoorOpenYellow:
                        CreateDoor(ButtonColor.Yellow, true, actorPosition);
                        break;
                    case ActorType.DoorOpenGreen:
                        CreateDoor(ButtonColor.Green, true, actorPosition);
                        break;
                    case ActorType.DoorOpenBlue:
                        CreateDoor(ButtonColor.Blue, true, actorPosition);
                        break;
                    case ActorType.ConveyorRight:
                        CreateConveyor(CardinalDirection.Right, actorPosition);
                        break;
                    case ActorType.ConveyorLeft:
                        CreateConveyor(CardinalDirection.Left, actorPosition);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void CreateConveyor(CardinalDirection direction, Vector2 actorPosition)
        {
            var actor = new Conveyor(this, direction);
            actor.Position = actorPosition;
            AddActor(actor);
        }

        private void CreateDoor(ButtonColor color, bool isOpenWhenButtonUp, Vector2 actorPosition)
        {
            var actor = new Door(this, color, isOpenWhenButtonUp);
            actor.Position = actorPosition;
            AddActor(actor);
        }

        private void CreateButton(ButtonColor color, Vector2 actorPosition)
        {
            var actor = new Button(this, color);
            actor.Position = actorPosition;
            AddActor(actor);
        }

        private void CreateElectrifiedFloor(Vector2 actorPosition)
        {
            var actor = new ElectrifiedFloor(this);
            actor.Position = actorPosition;
            AddActor(actor);
        }

        private void CreateBreakableWall(Vector2 actorPosition)
        {
            var actor = new BreakableWall(this);
            actor.Position = actorPosition;
            AddActor(actor);
        }

        private void CreateBomb(Vector2 actorPosition)
        {
            var actor = new Bomb(this);
            actor.Position = actorPosition;
            AddActor(actor);
        }

        private void CreateSteelBlock(Vector2 actorPosition)
        {
            var actor = new SteelBlock(this);
            actor.Position = actorPosition;
            AddActor(actor);
        }

        private void CreateCrate(Vector2 actorPosition)
        {
            var actor = new Crate(this);
            actor.Position = actorPosition;
            AddActor(actor);
        }

        private void CreateEndGoal(Vector2 actorPosition)
        {
            var actor = new EndGoal(this);
            actor.Position = actorPosition;
            AddActor(actor);
        }

        private void CreateSpawnPoint(Vector2 actorPosition)
        {
            var actor = new PlayerSpawnPoint(this);
            actor.Position = actorPosition;
            PlayerSpawnPoint = actorPosition;
            AddActor(actor);
        }

        public void SpawnPlayer()
        {
            if (PlayerBotsLeft == 0)
            {
                RestartLevel();
            }
            else
            {
                PlayerBotsLeft -= 1;
                Player?.RemoveGraphicsFromScene();
                Player = new Player(this);
                Player.Position = PlayerSpawnPoint;
                Player.AddGraphicsToScene();

                Camera.AnimateToTarget(Player.GetAttachedPositionedObject(), 0.5f, Easing.QuadraticEasing);
            }
        }

        private void RestartLevel()
        {
            LoadLevel(CurrentLevelNumber);
        }

        private void InitializeGraphics()
        {
            RenderEngine = new RenderEngine(_graphics, 320,240,true);
            Camera = new TransitionCamera
            {
                ViewPortWidth = 320,
                ViewPortHeight = 240
            };
            Camera.SetTarget(new Point(Camera.ViewPortWidth/2, Camera.ViewPortHeight/2));
            RenderEngine.Camera = Camera;

            BackgroundLayer = new Layer();
            BackActorLayer = new Layer();
            ActorLayer = new Layer();
            PlayerLayer = new Layer();
            HudLayer = new Layer() {IsHudLayer = true};

            RenderEngine.AddLayerToTop(BackgroundLayer);
            RenderEngine.AddLayerToTop(BackActorLayer);
            RenderEngine.AddLayerToTop(ActorLayer);
            RenderEngine.AddLayerToTop(PlayerLayer);
            RenderEngine.AddLayerToTop(HudLayer);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            HandleInput(gameTime);
            Player.Update(gameTime);
            UpdateActors(gameTime);
            UpdateHud(gameTime);

            RenderEngine.Update(gameTime);
            base.Update(gameTime);
        }

        private void UpdateHud(GameTime gameTime)
        {
            LevelText.Text = $"Level {CurrentLevelNumber + 1}";
            BotCountText.Text = $"Bots Remaining {PlayerBotsLeft}/{LevelSettings.Levels[CurrentLevelNumber].NumberOfBotsAvailable}";
        }

        private void UpdateActors(GameTime gameTime)
        {
            var playerCollisionBounds = Player.GetCollisionBounds();
            var actors = new List<ActorBase>(Actors);
            foreach (var actor in actors)
            {
                actor.Update(gameTime);
                var actorCollisionBounds = actor.GetCollisionBounds();
                if (actorCollisionBounds.Intersects(playerCollisionBounds))
                {
                    actor.HandleCollisionWithPlayer();
                }

                foreach (var otherActor in actors)
                {
                    if (actor == otherActor)
                    {
                        continue;
                    }

                    if (actorCollisionBounds.Intersects(otherActor.GetCollisionBounds()))
                    {
                        actor.HandleCollisionWithActor(otherActor);
                    }
                }
            }
        }

        private void HandleInput(GameTime gameTime)
        {
            var state = ControllerManager.Update(gameTime);

            if (state.Quit.IsJustPressed)
            {
                Exit();
            }

            if (state.Left.IsDown)
            {
                Player.TryWalkingLeft();
            }

            if (state.Right.IsDown)
            {
                Player.TryWalkingRight();
            }

            if (state.Up.IsDown)
            {
                Player.TryClimbUpLadder();
            }

            if (state.Down.IsDown)
            {
                Player.TryClimbDownLadder();
            }

            if (state.Deactivate.IsJustPressed)
            {
                Player.TryDeactivate();
            }

            if (state.Explode.IsJustPressed)
            {
                Player.TryExplode();
            }

            if (state.Reset.IsJustPressed)
            {
                RestartLevel();
            }

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            RenderEngine.Draw(gameTime);
            base.Draw(gameTime);
        }

        public void AddActor(ActorBase actor)
        {
            actor.AddGraphicsToScene();
            Actors.Add(actor);
        }

        public void RemoveActor(ActorBase actor)
        {
            actor.RemoveGraphicsFromScene();
            Actors.Remove(actor);
        }

        public void CompleteLevel()
        {
            LoadLevel(CurrentLevelNumber += 1);
        }

        public void UpdateButtonState(ButtonColor color)
        {
            ButtonStates[(int) color] = Actors.OfType<Button>().Any(a => a.Color == color && a.IsPressed);
        }
    }
}
