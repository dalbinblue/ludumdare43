﻿namespace DalbinBlue.LudumDare43.Sacrifices
{
    public class LevelSettings
    {
        public int NumberOfBotsAvailable { get; set; }
        public string HelpText { get; set; }


        public static LevelSettings[] Levels =
        {
            // Level - 0 Title screen level
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "" }, 

            // Level - 1 Tutorial deactivation
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "You can deactivate your robot with left control or A on a game pad to make a path for future robots." },

            // Level - 2 Tutorial explosions
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "You can also blow up your robot to remove obstacles with the space bar or X on a game pad." },

            // Level - 3 Tutorial electrified floors
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "Electrified floors will cause robots to explode on contact.  Cover them with boxes to pass." },

            // Level - 4 Tutorial switches
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "Buttons on the floor will open or close doors of same color.  Buttons can be pressed by standing on them or pushing something on them." },

            // Level - 5 Limited robots
            new LevelSettings {NumberOfBotsAvailable = 2, HelpText = "Sometimes you don't have many robots to spare.  If you use them all, the level will restart.  The number remaining is on the top right of the screen." },

            // Level - 6 Bombs part 1
            new LevelSettings {NumberOfBotsAvailable = 3, HelpText = "Bombs will explode if hit with another explosion, causing a chain reaction." },

            // Level - 7 Bombs part 2
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "If you mess up you can reset the level by pressing R on the keyboard or Y on the keypad." },

            // Level - 8 Three buttons part one
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "Three buttons." },

            // Level - 9 Three buttons part two
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "Hmm... This seems a little different." },

            // Level - 10 Three buttons part three
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "Now this is ridiculous." },

            // Level 11
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "" },

            // Level 12
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "" },

            // Level 13
            new LevelSettings {NumberOfBotsAvailable = 3, HelpText = "Hmm... You don't have enough robots to hit all the switches, how will you get across?" },

            // Level 14
            new LevelSettings {NumberOfBotsAvailable = 99, HelpText = "" },

            // Level 15
            new LevelSettings {NumberOfBotsAvailable = 10, HelpText = "You're reached the end of the dungeon, congrats!  Made in 48 hours by Dalbinblue for Ludum Dare 43." },

        };
    }
}